//
//  Repository.swift
//  
//
//  Created by Mohamed Abdelli on 14/08/2020.
//

import CoreData

struct Repository {
    
    static let shared = Repository()
    
    var context: NSManagedObjectContext
    
    private init() {
        let dataModel = WeatherDataModel()
        let container = NSPersistentContainer(name: "Weather", managedObjectModel: dataModel.model)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        context = container.viewContext
    }
    
    func save() {
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                if let error = error as NSError? {
                    fatalError("Unresolved error \(error), \(error.userInfo)")
                }
            }
        }
    }
    
    func getItems<T: NSManagedObject>(filterPredicate: NSPredicate? = nil) -> [T] {
        let request = T.fetchRequest()
        request.predicate = filterPredicate
        guard let items = try? context.fetch(request) as? [T] else {
            return []
        }
        
        return items
    }
    
    func deleteAll<T: NSManagedObject>(_ type: T.Type, filterPredicate: NSPredicate? = nil) {
        let items: [T] = self.getItems(filterPredicate: filterPredicate)
        for item in items {
            delete(item)
        }
    }
    
    func delete<T: NSManagedObject>(_ item: T) {
        context.delete(item)
    }
}
