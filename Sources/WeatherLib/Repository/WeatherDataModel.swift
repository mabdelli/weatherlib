//
//  File.swift
//  
//
//  Created by Mohamed Abdelli on 14/08/2020.
//

import CoreData

struct WeatherDataModel {
    
    var model: NSManagedObjectModel
    
    init() {
        model = NSManagedObjectModel()
        model.entities = createDataModel()
    }
    
    func createDataModel() -> [NSEntityDescription] {
        //Entities
        let cityEntity = NSEntityDescription()
        cityEntity.name = "City"
        cityEntity.managedObjectClassName = NSStringFromClass(CityManagedObject.self)
        
        let coordinateEntity = NSEntityDescription()
        coordinateEntity.name = "Coordinate"
        coordinateEntity.managedObjectClassName = NSStringFromClass(CoordinateManagedObject.self)
        
        let forecastEntity = NSEntityDescription()
        forecastEntity.name = "Forecast"
        forecastEntity.managedObjectClassName = NSStringFromClass(Forecast.self)
        
        //Attributes
        let id = NSAttributeDescription()
        id.name = "id"
        id.attributeType = .integer64AttributeType
        
        let name = NSAttributeDescription()
        name.name = "name"
        name.attributeType = .stringAttributeType
        name.isOptional = true
        
        let country = NSAttributeDescription()
        country.name = "country"
        country.attributeType = .stringAttributeType
        country.isOptional = true
        
        let latitude = NSAttributeDescription()
        latitude.name = "latitude"
        latitude.attributeType = .floatAttributeType
        
        let longitude = NSAttributeDescription()
        longitude.name = "longitude"
        longitude.attributeType = .floatAttributeType
        
        let temperatureCity = NSAttributeDescription()
        temperatureCity.name = "temperature"
        temperatureCity.attributeType = .floatAttributeType
        
        let temperatureForecast = NSAttributeDescription()
        temperatureForecast.name = "temperature"
        temperatureForecast.attributeType = .floatAttributeType
        
        let pressure = NSAttributeDescription()
        pressure.name = "pressure"
        pressure.attributeType = .integer64AttributeType
        
        let humidity = NSAttributeDescription()
        humidity.name = "humidity"
        humidity.attributeType = .integer64AttributeType
        
        let windSpeed = NSAttributeDescription()
        windSpeed.name = "windSpeed"
        windSpeed.attributeType = .floatAttributeType
        
        let windDeg = NSAttributeDescription()
        windDeg.name = "windDeg"
        windDeg.attributeType = .integer64AttributeType
        
        let icon = NSAttributeDescription()
        icon.name = "icons"
        icon.attributeType = .transformableAttributeType
        icon.valueTransformerName = "NSSecureUnarchiveFromData"
        
        //Relationships
        let coordinateToCity = NSRelationshipDescription()
        coordinateToCity.name = "coordinate"
        coordinateToCity.destinationEntity = coordinateEntity
        coordinateToCity.maxCount = 1
        coordinateToCity.isOptional = true
        coordinateToCity.deleteRule = .cascadeDeleteRule
        
        let cityToCoordinate = NSRelationshipDescription()
        cityToCoordinate.name = "city"
        cityToCoordinate.destinationEntity = cityEntity
        cityToCoordinate.maxCount = 1
        cityToCoordinate.isOptional = true
        cityToCoordinate.deleteRule = .nullifyDeleteRule
        
        coordinateToCity.inverseRelationship = cityToCoordinate
        cityToCoordinate.inverseRelationship = coordinateToCity
        
        let forecastToCity = NSRelationshipDescription()
        forecastToCity.name = "forecast"
        forecastToCity.destinationEntity = forecastEntity
        forecastToCity.maxCount = 1
        forecastToCity.isOptional = true
        forecastToCity.deleteRule = .cascadeDeleteRule
        
        let cityToForecast = NSRelationshipDescription()
        cityToForecast.name = "city"
        cityToForecast.destinationEntity = cityEntity
        cityToForecast.maxCount = 1
        cityToForecast.isOptional = true
        cityToForecast.deleteRule = .nullifyDeleteRule
        
        cityToForecast.inverseRelationship = forecastToCity
        forecastToCity.inverseRelationship = cityToForecast
        
        //Properties
        cityEntity.properties = [id, name, country, coordinateToCity, temperatureCity, forecastToCity]
        coordinateEntity.properties = [latitude, longitude, cityToCoordinate]
        forecastEntity.properties = [temperatureForecast, pressure, humidity, windSpeed, windDeg, icon, cityToForecast]

        return [cityEntity, coordinateEntity, forecastEntity]
    }
}


