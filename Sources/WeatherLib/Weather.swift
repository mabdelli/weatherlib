import CoreData

public struct Weather {
    static let env = "https://api.openweathermap.org"
    private var key: String
    private let networkManager = NetworkManager(with: Network())
    
    public init(key: String) {
        self.key = key
    }
    
    @discardableResult
    public func getForecast(latitude: Float, longitude: Float, completion: @escaping (Result<Forecast?, Error>)->Void) -> Cancellable? {
        let coordinates: [CoordinateManagedObject] = Repository.shared.getItems(filterPredicate: NSPredicate(format: "latitude == %f AND longitude == %f", latitude, longitude))
        if networkManager.network.isNetworkAvailable() {
            return networkManager.retrieveData(api: .getForecast(latitude: latitude, longitude: longitude, appId: key)) { [completion] result in
                switch result {
                case .success(let data):
                    guard let data = data else {
                        completion(.success(nil))
                        return
                    }
                    let jsonDecoder = JSONDecoder()
                    jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
                    do {
                        if let forecast = coordinates.first?.city?.forecast {
                            Repository.shared.delete(forecast)
                        }
                        let forecast = try jsonDecoder.decode(Forecast.self, from: data)
                        coordinates.first?.city?.forecast = forecast
                        Repository.shared.save()
                        completion(.success(forecast))
                    } catch (let error) {
                        completion(.failure(.otherServerError(error.localizedDescription)))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } else {
            if !coordinates.isEmpty {
                completion(.success(coordinates.first?.city?.forecast))
            } else {
                completion(.failure(.noNetwork))
            }
            return nil
        }
    }
    
    @discardableResult
    public func getCity(value: String, completion: @escaping (Result<[City]?, Error>)->Void) -> Cancellable? {
        if networkManager.network.isNetworkAvailable() {
            return networkManager.retrieveData(api: .getCities(value: value, appId: key)) { result in
                switch result {
                case .success(let data):
                    guard let data = data else {
                        completion(.success(nil))
                        return
                    }
                    let jsonDecoder = JSONDecoder()
                    do {
                        let item = try
                        jsonDecoder.decode(Cities.self, from: data)
                        completion(.success(item.list))
                    } catch (let error) {
                        completion(.failure(.otherServerError(error.localizedDescription)))
                    }
                case .failure(let error):
                    completion(.failure(error))
                }
            }
        } else {
            completion(.failure(.noNetwork))
            return nil
        }
    }
    
    public func saveFavoriteCity(_ city: City) {
        
        let cities: [CityManagedObject] = Repository.shared.getItems(filterPredicate: NSPredicate(format: "id == %d", city.id))
        if cities.isEmpty {
            CityManagedObject(with: city)
            Repository.shared.save()
        }
    }
    
    public func deleteFavoriteCity(_ city: City) {
        let cities: [CityManagedObject] = Repository.shared.getItems(filterPredicate: NSPredicate(format: "id == %d", city.id))
        if !cities.isEmpty, let cityManagedObject = cities.first {
            Repository.shared.delete(cityManagedObject)
            Repository.shared.save()
        }
    }
    
    @discardableResult
    public func loadFavoritesCities(refresh: Bool, completion: @escaping ([City], Error?)->Void) -> Cancellable? {
        var citiesManagedObject: [CityManagedObject] {
            Repository.shared.getItems()
        }
        var favoritesCities: [City] {
            citiesManagedObject.map { City(with: $0) }
        }
        let idsArray = citiesManagedObject.map { "\($0.id)" }

        if !refresh || idsArray.isEmpty {
            completion(favoritesCities, nil)
            return nil
        }
    
        let ids = idsArray.joined(separator: ",")
        
        if networkManager.network.isNetworkAvailable() {
            return networkManager.retrieveData(api: .getGroupCities(ids: ids, appId: key)) { result in
                switch result {
                case .success(let data):
                    guard let data = data else {
                        completion(favoritesCities, nil)
                        return
                    }
                    let jsonDecoder = JSONDecoder()
                    do {
                        let item = try
                        jsonDecoder.decode(Cities.self, from: data)
                        if let cities = item.list {
                            for city in cities {
                                let matchCity = citiesManagedObject.first { $0.id == city.id }
                                matchCity?.temperature = city.temperature
                            }
                            Repository.shared.save()
                        }
                        completion(favoritesCities, nil)
                    } catch (let error) {
                        completion(favoritesCities, .otherServerError(error.localizedDescription))
                    }
                case .failure(let error):
                    completion(favoritesCities, error)
                }
            }
        } else {
            completion(favoritesCities, .noNetwork)
            return nil
        }
    }
}
