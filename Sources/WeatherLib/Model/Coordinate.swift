//
//  Coordinate.swift
//  
//
//  Created by Mohamed Abdelli on 12/08/2020.
//

import CoreData

public struct Coordinate: Decodable {
    
    public var longitude: Float
    public var latitude: Float

    enum CoordinateCodingKey: String, CodingKey {
        case lat
        case lon
    }
    
    init(with coordinate: CoordinateManagedObject) {
        self.latitude = coordinate.latitude
        self.longitude = coordinate.longitude
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CoordinateCodingKey.self)
        latitude = try container.decodeIfPresent(Float.self, forKey: .lat) ?? 0
        longitude = try container.decodeIfPresent(Float.self, forKey: .lon) ?? 0
    }
}

class CoordinateManagedObject: NSManagedObject {
    @NSManaged var longitude: Float
    @NSManaged var latitude: Float
    @NSManaged var city: CityManagedObject?
    
    convenience init(with coordinate: Coordinate) {
        guard let coordinateEntity = NSEntityDescription.entity(forEntityName: "Coordinate", in: Repository.shared.context) else {
            fatalError("Failed to get Coordinate Entity")
        }
        self.init(entity: coordinateEntity, insertInto: Repository.shared.context)
        self.latitude = coordinate.latitude
        self.longitude = coordinate.longitude
    }
}
