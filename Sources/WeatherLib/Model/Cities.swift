//
//  Cities.swift
//  
//
//  Created by Mohamed Abdelli on 12/08/2020.
//

import CoreData

public enum Temperature: String {
    case celsius = "°C"
    case kelvin = "K"
    
    public func format(value: Float) -> String {
        String(format: "%.2f \(self.rawValue)", self == .celsius ? value-273.15 : value) //273.15 is the difference between K and C° value
    }
}

public struct Cities: Decodable {
    public var list: [City]?
}

public struct City: Decodable {

    public var id: Int
    public var name: String?
    public var country: String?
    public var coordinate: Coordinate?
    public var temperature: Float //Kelvin
    
    enum CityCodingKey: String, CodingKey {
        case id
        case name
        case coord
        case main
        case sys
    }
    
    enum MainCodingKey: String, CodingKey {
        case temp
    }
    
    enum SysCodingKey: String, CodingKey {
        case country
    }
    
    init(with city: CityManagedObject) {
        self.id = city.id
        self.name = city.name
        self.country = city.country
        if let coordinate = city.coordinate {
            self.coordinate = Coordinate(with: coordinate)
        }
        self.temperature = city.temperature
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CityCodingKey.self)
        id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        name = try container.decodeIfPresent(String.self, forKey: .name)
        coordinate = try container.decodeIfPresent(Coordinate.self, forKey: .coord)
        
        let main = try container.nestedContainer(keyedBy: MainCodingKey.self, forKey: .main)
        temperature = try main.decodeIfPresent(Float.self, forKey: .temp) ?? Float.nan
        
        let sys = try container.nestedContainer(keyedBy: SysCodingKey.self, forKey: .sys)
        country = try sys.decodeIfPresent(String.self, forKey: .country)
    }
}

class CityManagedObject: NSManagedObject {
    @NSManaged var id: Int
    @NSManaged var name: String?
    @NSManaged var country: String?
    @NSManaged var coordinate: CoordinateManagedObject?
    @NSManaged var temperature: Float
    @NSManaged var forecast: Forecast?

    @discardableResult
    convenience init(with city: City) {
        guard let cityEntity = NSEntityDescription.entity(forEntityName: "City", in: Repository.shared.context) else {
                fatalError("Failed to get City Entity")
        }
        self.init(entity: cityEntity, insertInto: Repository.shared.context)
        self.id = city.id
        self.name = city.name
        self.country = city.country
        if let coordinate = city.coordinate {
            self.coordinate = CoordinateManagedObject(with: coordinate)
            self.coordinate?.city = self
        }
        self.temperature = city.temperature
    }
}
