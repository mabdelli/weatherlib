//
//  Forecast.swift
//  
//
//  Created by Mohamed Abdelli on 11/08/2020.
//

import CoreData

public class Forecast: NSManagedObject, Decodable {
    @NSManaged public var temperature: Float
    @NSManaged public var pressure: Int
    @NSManaged public var humidity: Int
    @NSManaged public var windSpeed: Float
    @NSManaged public var windDeg: Int
    @NSManaged public var icons: [String]
    @NSManaged var city: CityManagedObject?
    
    enum ForecastCodingKey: CodingKey {
        case current
    }
    
    enum CurrentCodingKey: CodingKey {
        case feelsLike
        case pressure
        case humidity
        case windSpeed
        case windDeg
        case weather
    }
    
    public required convenience init(from decoder: Decoder) throws {
        guard let entity = NSEntityDescription.entity(forEntityName: "Forecast", in: Repository.shared.context) else {
                fatalError("Failed to decode Forecast")
        }
        self.init(entity: entity, insertInto: Repository.shared.context)
        
        let forecast = try decoder.container(keyedBy: ForecastCodingKey.self)
        
        let current = try forecast.nestedContainer(keyedBy: CurrentCodingKey.self, forKey: .current)
        temperature = try current.decodeIfPresent(Float.self, forKey: .feelsLike) ?? Float.nan
        pressure = try current.decodeIfPresent(Int.self, forKey: .pressure) ?? 0
        humidity = try current.decodeIfPresent(Int.self, forKey: .humidity) ?? 0
        windSpeed = try current.decodeIfPresent(Float.self, forKey: .windSpeed) ?? Float.nan
        windDeg = try current.decodeIfPresent(Int.self, forKey: .windDeg) ?? 0
        
        let iconArray = try current.decodeIfPresent([Icon].self, forKey: .weather)
        icons = iconArray?.map { "https://openweathermap.org/img/wn/\($0.icon)@2x.png" } ?? []
    }
}

struct Icon: Decodable {
    var icon: String
}
