//
//  Network.swift
//  
//
//  Created by Mohamed Abdelli on 11/08/2020.
//

import Foundation
import SystemConfiguration

protocol RequestDefinition {
    var baseURL: String { get }
    var path: String { get }
    var method: Network.Method { get }
    var headers: [String: String]? { get }
    var query: [String:String]? { get }
    var body: Data? { get }
}

class Network {
    typealias NetworkResponse = (Data?, HTTPURLResponse?)

    enum Method: String {
        case get = "GET"
        case post = "POST"
        case put = "PUT"
        case delete = "DELETE"
    }
    
    func request(for req: RequestDefinition, completion: @escaping (Result<NetworkResponse, Swift.Error>) -> Void) -> Cancellable? {
        let urlSession = URLSession(configuration: .default)
        guard var url = URL(string: req.baseURL) else {
            completion(.failure(Error.invalidURL))
            return nil
        }
        if !req.path.isEmpty {
            url = url.appendingPathComponent(req.path)
        }
        if let query = req.query {
            
            var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)
            var queryItems: [URLQueryItem] = []
            for key in query.keys {
                let queryItem = URLQueryItem(name: key, value: query[key])
                queryItems.append(queryItem)
            }
            urlComponents?.queryItems = queryItems
            url = urlComponents?.url ?? url
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = req.method.rawValue
        urlRequest.httpBody = req.body
        if let headers = req.headers {
            for key in headers.keys {
                urlRequest.setValue(headers[key], forHTTPHeaderField: key)
            }
        }

        let task = urlSession.dataTask(with: urlRequest) { (data, urlResponse, error) in
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success((data, urlResponse as? HTTPURLResponse)))
                }
            }
        }
        task.resume()
        return task
    }
    
    func isNetworkAvailable() -> Bool {
        guard let reachability = SCNetworkReachabilityCreateWithName(nil, Weather.env) else {
            return false
        }

        var flags: SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(reachability, &flags) {
            return false
        }

        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)

        return (isReachable && !needsConnection)
    }
}
