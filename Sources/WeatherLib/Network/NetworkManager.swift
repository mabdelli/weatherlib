//
//  NetworkManager.swift
//  
//
//  Created by Mohamed Abdelli on 11/08/2020.
//

import Foundation

class NetworkManager {
    
    let network: Network
    
    init(with network: Network = Network()) {
        self.network = network
    }
       
    @discardableResult
    func retrieveData(api: OpenWeatherMapAPI, completion: @escaping (Result<Data?, Error>) -> Void) -> Cancellable? {
        self.network.request(for: api) { (result) in
            switch result {
            case .success((let data, let response)):
                if let statusCode = response?.statusCode {
                    if (200..<400).contains(statusCode) {
                        completion(.success(data))
                    } else {
                        completion(.failure(Error.error(statusCode)))
                    }
                } else {
                    completion(.failure(Error.defaultError))
                }
            case .failure(let error):
                completion(.failure(Error.otherServerError(error.localizedDescription)))
            }
        }
    }
}
