//
//  OpenWeatherMapAPI.swift
//  
//
//  Created by Mohamed Abdelli on 11/08/2020.
//

import Foundation

enum OpenWeatherMapAPI {
    case getForecast(latitude: Float, longitude: Float, appId: String)
    case getCities(value: String, appId: String)
    case getGroupCities(ids: String, appId: String)
    case getIcon(url: String)
}

extension OpenWeatherMapAPI: RequestDefinition {
    var baseURL: String {
        switch self {
        case .getIcon(let url):
            return url
        default:
            return Weather.env
        }
        
    }
    
    var path: String {
        switch self {
        case .getForecast:
            return "data/2.5/onecall"
        case .getCities:
            return "data/2.5/find"
        case .getGroupCities:
            return "data/2.5/group"
        default:
            return ""
        }
    }
    
    var method: Network.Method { .get }
    
    var headers: [String : String]? { nil }
    
    var query: [String : String]? {
        switch self {
        case .getForecast(let latitude, let longitude, let appId):
            return ["lat":"\(latitude)",
                    "lon":"\(longitude)",
                "exclude":"hourly,minutely,daily",
                    "appid":appId]
        case .getCities(let value, let appId):
            return ["q":value,
                    "appid":appId]
        case .getGroupCities(let ids, let appId):
            return ["id":ids,
                    "appid":appId]
        default:
            return nil
        }
    }
    
    var body: Data? { nil }
}
