//
//  URLSessionTask.swift
//  
//
//  Created by Mohamed Abdelli on 11/08/2020.
//

import Foundation

public protocol Cancellable {
    func cancelRequest()
}

extension URLSessionTask: Cancellable {
    public func cancelRequest() {
        cancel()
    }
}
