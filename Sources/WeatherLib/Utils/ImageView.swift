//
//  ImageView.swift
//  
//
//  Created by Mohamed Abdelli on 16/08/2020.
//

#if !os(macOS)
import UIKit

public class ImageView: UIImageView {

    public var placeholder: UIImage?
    public var error: UIImage?

    let activityIndicatorView = UIActivityIndicatorView(style: .gray)
    static let cache = NSCache<NSString, NSData>()

    public override func awakeFromNib() {
        activityIndicatorView.hidesWhenStopped = true
        self.addSubview(activityIndicatorView)
        activityIndicatorView.center = CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    public func download(from url: String?) {
        guard let url = url else {
            image = placeholder
            return
        }
        
        activityIndicatorView.startAnimating()
        let networkManager = NetworkManager()
        if let data = ImageView.cache.object(forKey: url as NSString) {
            image = UIImage(data: data as Data)
            activityIndicatorView.stopAnimating()
        } else if networkManager.network.isNetworkAvailable() {
            networkManager.retrieveData(api: .getIcon(url: url)) { [weak self] (result) in
                switch result {
                case .success(let data):
                    if let data = data {
                        ImageView.cache.setObject(data as NSData, forKey: url as NSString)
                        self?.image = UIImage(data: data) ?? self?.placeholder
                    }
                case .failure(let error):
                    self?.image = self?.error
                }
                self?.activityIndicatorView.stopAnimating()
            }
        } else {
            activityIndicatorView.stopAnimating()
            image = placeholder
        }
    }

}
#endif
