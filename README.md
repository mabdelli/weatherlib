# WeatherLib

### Description

WeatherLib is a mobile library for iOS which allow to easily process informations returned by the [OpenWeatherMap API](https://openweathermap.org/api/one-call-api).

### Requirements

Before using the library you need to sign in on OpenWeatherMap web site and generate an API key. Visit [OpenWeatherMap](https://home.openweathermap.org/)

iOS>=11, macOS>=10.12

### Usage

To get cities by name:
```swift
let weather = Weather(key: /*YOUR API KEY*/)
weather.getCity(city: cityValue) { result in
    switch result {
        case .success(let cities): //cities is a list of City
        
        case .failure(let error):
    }
}
```

To get forecast from coordinate:
```swift
let weather = Weather(key: /*YOUR API KEY*/)
weather.getForecast(latitude: latitudeValue, longitude: longitudeValue) { result in
    switch result {
        case .success(let forecast):
        //Forecast.class
        //temperature -> Kelvin value by default. Use Temperature enum to format.
        //pressure -> hPA
        //humidity -> %
        //windSpeed -> m/s
        //windDeg -> Degree
        case .failure(let error):
    }
}
```

To save a city locally:
```swift
let weather = Weather(key: /*YOUR API KEY*/)
weather.saveFavoriteCity(cityValue) //cityValue -> City.class
```

To delete a city:
```swift
let weather = Weather(key: /*YOUR API KEY*/)
weather.deleteFavoriteCity(cityValue) //cityValue -> City.class
```

To load all favorites cities:
```swift
let weather = Weather(key: /*YOUR API KEY*/)
//Inform with refreshValue if you need to update your favorites cities locally
weather.loadFavoritesCities(refresh: refreshValue) { (cities, error) in
    //cities -> a list of City
}
```

### Framework

WeatherLib.xcframework is available, dezip the WeatherLib.xcframework.zip at the root of the repo and add it to "Frameworks, Librairies, and Embedded Content" section on the right target.

### Swift Package Manager

To integrate using Apple's Swift package manager, add the following as a dependency to your `Package.swift`:

```swift
.package(url: "https://mabdelli@bitbucket.org/mabdelli/weatherlib.git", .upToNextMajor(from: "1.0.0"))
```

and then specify `"WeatherLib"` as a dependency of the Target in which you wish to use it.
Here's an example `PackageDescription`:

```swift
// swift-tools-version:5.2
import PackageDescription

let package = Package(
    name: "MyPackage",
    products: [
        .library(
            name: "MyPackage",
            targets: ["MyPackage"]),
    ],
    dependencies: [
        .package(url: "https://mabdelli@bitbucket.org/mabdelli/weatherlib.git", .upToNextMajor(from: "1.0.0"))
    ],
    targets: [
        .target(
            name: "MyPackage")
    ]
)
```
