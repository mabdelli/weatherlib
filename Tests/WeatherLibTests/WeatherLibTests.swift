import XCTest
@testable import WeatherLib

final class WeatherLibTests: XCTestCase {
    
    func testCity() {
        let json = """
{
    "coord": {
        "lon": -0.13,
        "lat": 51.51
    },
    "weather": [{
        "id": 300,
        "main": "Drizzle",
        "description": "light intensity drizzle",
        "icon": "09d"
    }],
    "base": "stations",
    "main": {
        "temp": 280.32,
        "pressure": 1012,
        "humidity": 81,
        "temp_min": 279.15,
        "temp_max": 281.15
    },
    "visibility": 10000,
    "wind": {
        "speed": 4.1,
        "deg": 80
    },
    "clouds": {
        "all": 90
    },
    "dt": 1485789600,
    "sys": {
        "type": 1,
        "id": 5091,
        "message": 0.0103,
        "country": "GB",
        "sunrise": 1485762037,
        "sunset": 1485794875
    },
    "id": 2643743,
    "name": "London",
    "cod": 200
}
"""
        let jsonDecoder = JSONDecoder()
        let city = try! jsonDecoder.decode(City.self, from: json.data(using: .utf8)!)
        XCTAssertNotNil(city)
        XCTAssertNotNil(city.coordinate)
        XCTAssertNotNil(city.country)
        XCTAssertNotNil(city.name)
        XCTAssertEqual(city.id, 2643743)
        XCTAssertEqual(city.temperature, 280.32)
        XCTAssertEqual(city.coordinate!.longitude, -0.13)
        XCTAssertEqual(city.coordinate!.latitude, 51.51)
        XCTAssertEqual(city.country, "GB")
        XCTAssertEqual(city.name, "London")

    }
    
    func testForecast() {
        let json = """
{
    "lat": 33.44,
    "lon": -94.04,
    "timezone": "America/Chicago",
    "timezone_offset": -18000,
    "current": {
        "dt": 1597523875,
        "sunrise": 1597491545,
        "sunset": 1597539734,
        "temp": 306.57,
        "feels_like": 312.43,
        "pressure": 1014,
        "humidity": 62,
        "dew_point": 298.29,
        "uvi": 10.22,
        "clouds": 1,
        "visibility": 10000,
        "wind_speed": 0.91,
        "wind_deg": 84,
        "weather": [{
            "id": 800,
            "main": "Clear",
            "description": "clear sky",
            "icon": "01d"
        }]
    }
}
"""
        let jsonDecoder = JSONDecoder()
        jsonDecoder.keyDecodingStrategy = .convertFromSnakeCase
        let forecast = try! jsonDecoder.decode(Forecast.self, from: json.data(using: .utf8)!)
        XCTAssertNotNil(forecast)
        XCTAssertEqual(forecast.humidity, 62)
        XCTAssertEqual(forecast.temperature, 312.43)
        XCTAssertEqual(forecast.pressure, 1014)
        XCTAssertEqual(forecast.windSpeed, 0.91)
        XCTAssertEqual(forecast.windDeg, 84)
        XCTAssertEqual(forecast.icons.count, 1)
        XCTAssertEqual(forecast.icons.first, "https://openweathermap.org/img/wn/01d@2x.png")
    }
    
    func testTemperatureFormat() {
        XCTAssertEqual(Temperature.celsius.format(value: 273.15), "0.00 °C")
        XCTAssertEqual(Temperature.kelvin.format(value: 273.150000), "273.15 K")

    }
    
    func testErrors() {
        XCTAssertEqual(Error.error(400), Error.httpError400)
        XCTAssertEqual(Error.error(401), Error.httpError401)
        XCTAssertEqual(Error.error(403), Error.httpError403)
        XCTAssertEqual(Error.error(404), Error.httpError404)
        XCTAssertEqual(Error.error(408), Error.httpError408)
        XCTAssertEqual(Error.error(417), Error.httpError417)
        XCTAssertEqual(Error.error(500), Error.httpError500)
        XCTAssertEqual(Error.error(999), Error.defaultError)
    }

    static var allTests = [
        ("testCity", testCity),
        ("testForecast", testForecast),
        ("testTemperatureFormat", testTemperatureFormat),
        ("testErrors", testErrors),
    ]
}
