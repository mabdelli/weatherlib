import XCTest

import WeatherLibTests

var tests = [XCTestCaseEntry]()
tests += WeatherLibTests.allTests()
XCTMain(tests)
